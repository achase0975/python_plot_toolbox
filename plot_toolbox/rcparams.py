# CUSTOMIZATION OF MATPLOTLIB FIGURE FOR QUALITY PRINTING
RCPARAMS = {
  "pgf.texsystem": "xelatex",
  # "pgf.texsystem": "lualatex",
  "font.family": "serif", # use serif/main font for text elements
  "font.serif":"Times",
  "text.usetex": True,    # use inline math for ticks
  "pgf.rcfonts": False,   # don't setup fonts from rc parameters  
  "pgf.preamble": "\n".join([
      r"\usepackage{fontspec}",       # Set font
      r"\usepackage{amsmath}",        # Maths symbol
      r"\setmainfont{XCharter}",      # Charter font
      r"\usepackage{pgfplots}",        # Pgfplots
      r"\pgfplotsset{width=10cm,compat=1.6}",
      r"\usepgfplotslibrary{external}",
      r"\tikzexternalize",
      ]),

  "font.size"       : 10,
  "xtick.labelsize" : 8,
  "ytick.labelsize" : 8,
  "figure.titlesize": "large",
  "axes.labelsize"  : 10,
  "axes.titlesize"  : "large",  # fontsize of the axes title
  "axes.labelpad"   : 0.01,     # space between label and axis
  "axes.labelweight": "normal",  # weight of the x and y labels
  "axes.labelcolor" : "black",
  "axes.xmargin"    : 0.01, #.05,  # x margin.  
  "axes.ymargin"    : 0.05, #.05,  # y margin 
  "axes.spines.left"   : True,   # display axis spines
  "axes.spines.bottom" : True,
  "axes.spines.top"    : True,
  "axes.spines.right"  : True,
}
