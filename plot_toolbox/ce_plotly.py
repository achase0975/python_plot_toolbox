import plotly.express as px
import plotly.graph_objects as go
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import numpy as np  # <- conda install -c anaconda numpy
from stl import mesh  # <- conda install -c conda-forge numpy-stl
from skimage import io
from plotly.subplots import make_subplots


def stl2mesh3d(stl_mesh):
    # stl_mesh is read by nympy-stl from a stl file; it is  an array of faces/triangles (i.e. three 3d points)
    # this function extracts the unique vertices and the lists I, J, K to define a Plotly mesh3d
    p, q, r = stl_mesh.vectors.shape  # (p, 3, 3)
    # the array stl_mesh.vectors.reshape(p*q, r) can contain multiple copies of the same vertex;
    # extract unique vertices from all mesh triangles
    vertices, ixr = np.unique(
        stl_mesh.vectors.reshape(p * q, r), return_inverse=True, axis=0
    )
    I = np.take(ixr, [3 * k for k in range(p)])
    J = np.take(ixr, [3 * k + 1 for k in range(p)])
    K = np.take(ixr, [3 * k + 2 for k in range(p)])
    return vertices, I, J, K


def create_mesh3D(
    stl_file="scapula.stl",
    title="Mesh3d from a STL file<br>AT&T building",
    colorscale=None,
    return_mesh=False,
):

    if colorscale is None:
        colorscale = [[0, "#e5dee5"], [1, "#e5dee5"]]
    vertices, I, J, K = stl2mesh3d(mesh.Mesh.from_file(stl_file))
    x, y, z = vertices.T
    mesh3D = go.Mesh3d(
        x=x,
        y=y,
        z=z,
        i=I,
        j=J,
        k=K,
        flatshading=True,
        colorscale=colorscale,
        intensity=z,
        name="AT&T",
        showscale=False,
    )

    layout = go.Layout(
        paper_bgcolor="rgb(1,1,1)",
        title_text=title,
        title_x=0.5,
        font_color="white",
        width=800,
        height=800,
        scene_camera=dict(eye=dict(x=1.25, y=-1.25, z=1)),
        scene_xaxis_visible=False,
        scene_yaxis_visible=False,
        scene_zaxis_visible=False,
    )
    fig = go.Figure(data=[mesh3D], layout=layout)

    lighting_effects = dict(
        ambient=0.4,
        diffuse=1,
        roughness=0.1,
        specular=1,
        fresnel=0.1,
        facenormalsepsilon=0,
    )
    fig.data[0].update(lighting=lighting_effects)
    norm_light = 3000
    light_vec = np.array([1, 1, 1]) * norm_light
    fig.data[0].update(
        lightposition=dict(x=light_vec[0], y=light_vec[1], z=light_vec[2])
    )

    if return_mesh:
        return fig, mesh3D
    else:
        return fig


def add_points(points, name, fig=None, scale=1, return_sct=False, *args, **kwargs):
    if points.shape[0] == 1:
        points = np.concatenate([points, points]).reshape(-1, 3)
    sct = go.Scatter3d(
        x=points[:, 0] * scale,
        y=points[:, 1] * scale,
        z=points[:, 2] * scale,
        name=name,
        mode="markers",
        **kwargs
    )
    if fig is None:
        fig = go.Figure(data=[sct])
    fig.add_trace(sct)
    return (fig, sct) if return_sct else fig


def dash_figure(fig):
    app = dash.Dash()
    app.layout = html.Div(
        [
            html.Div(id="output"),  # use to print current relayout values
            dcc.Graph(id="fig", figure=fig),
        ]
    )

    @app.callback(Output("output", "children"), Input("fig", "relayoutData"))
    def show_data(data):
        # show camera settings like eye upon change
        return [str(data)]

    app.run_server(debug=False, use_reloader=False)
    
    
    
    
def set_default_figure(fig=None, bcolor = "rgb(225, 225, 225)", gridcolor = "rgb(0, 0, 0)"):  
    fig.update_layout(
        scene_aspectmode="data",
        scene_camera={
            "up": {
                "x": -0.1158525218946756,
                "y": -0.65074497945838,
                "z": -0.7504060000295556,
            },
            "center": {
                "x": 0.5336066340193244,
                "y": 0.12181145357337546,
                "z": -0.04933289270599285,
            },
            "eye": {
                "x": -1.765254915630703,
                "y": -0.41288975424229607,
                "z": 0.7692680173158395,
            },
        },

        scene=dict(
            xaxis=dict(
                backgroundcolor=bcolor,
                gridcolor=gridcolor,
                visible=True,
                showbackground=True,
                zerolinecolor=gridcolor,
            ),
            yaxis=dict(
                backgroundcolor=bcolor,
                gridcolor=gridcolor,
                visible=True,
                showbackground=True,
                zerolinecolor=gridcolor,
            ),
            zaxis=dict(
                backgroundcolor=bcolor,
                gridcolor=gridcolor,
                visible=True,
                showbackground=True,
                zerolinecolor=gridcolor,
            ),
        ),
        autosize=True,
        # width=500,
        # height=500,
        # margin=dict(r=10, l=10, b=10, t=10),
        # legend=dict(yanchor="bottom", y=0.1, xanchor="left", x=0.80)
    )
    return fig